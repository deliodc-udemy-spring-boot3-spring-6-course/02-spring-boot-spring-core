package com.deliodc.springcoredemo.common;

public interface Coach {

    String getDailyWorkout();
}
