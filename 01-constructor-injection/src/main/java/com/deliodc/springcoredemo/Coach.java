package com.deliodc.springcoredemo;

public interface Coach {

    String getDailyWorkout();
}
